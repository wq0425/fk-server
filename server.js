const express = require('express');
const Mock = require('mockjs');
const cors = require('cors');
const path = require('path'); // 导入 path 模块
const fs = require('fs');
const jwt = require('jsonwebtoken');
const bodyParser = require("body-parser");

const app = express();
const port = 9000;
const secretKey = 'token';

// 定义白名单路径
const whitelist = ['/login']; // 可以根据需要添加其他路径

app.use(cors());

// 解析 url-encoded格式的表单数据
app.use(bodyParser.urlencoded({
  extended: false
}));

// 解析json格式的表单数据
app.use(bodyParser.json());

let file = path.join(__dirname, 'data.json'); //文件路径，__dirname为当前运行js文件的目录

// 保存数据
let tableData = {};

// 验证 token
const authenticateJWT = (req, res, next) => {
  // 检查请求路径是否在白名单中
  if (whitelist.includes(req.path)) {
    return next();
  }
  const authHeader = req.headers['authorization'];
  let token;
  if (authHeader && authHeader.startsWith('Bearer ')) {
    token = authHeader.split(' ')[1]; // 提取 Bearer 后面的 token
  } else {
    token = authHeader; // 如果没有 Bearer 前缀，直接使用整个 header 作为 token
  }
  if (token) {
    jwt.verify(token, secretKey, (err, decoded) => {
      if (err) {
        return res.status(403).send('无效的 token');
      }
      req.user = decoded;
      next();
    });
  } else {
    res.status(401).send('未提供 token');
  }
};

// 使用 JWT 验证中间件
app.use(authenticateJWT);

// 登陆
app.post('/login', (req, res) => {
  const { username, password } = req.body;
  fs.readFile(file, 'utf-8', function (err, data) {
    if (err) {
      res.status(500).send('文件读取失败');
    } else {
      const user = JSON.parse(data).users.find(u => u.username === username && u.password === password);
      if (user) {
        // 生成 JWT
        const token = jwt.sign({ username: user.username }, secretKey, { expiresIn: '5h' });
        const template = {
          code: 200,
          data: {
            msg: '登录成功',
            token: token // 这里可以生成一个token，这里简化为固定字符串
          }
        };
        const data = Mock.mock(template);
        res.json(data);
      } else {
        const template = {
          code: 500,
          data: {
            msg: '用户名或密码错误'
          }
        };
        const data = Mock.mock(template);
        res.status(200).json(data);
      }
    }
  });
});

// 获取数据
app.get('/fkData', (req, res) => {
  //读取json文件
  fs.readFile(file, 'utf-8', function (err, data) {
    if (err) {
      res.send('文件读取失败');
    } else {
      tableData = JSON.parse(data);
      res.send(data);
    }
  });
});

// 添加数据
app.post('/addTable', (req, res) => {
  tableData.data.map(item => {
    if (item.id === req.body.titleId) {
      item.list.unshift({
        id: req.body.id,
        name: req.body.name,
        quotas: req.body.quotas,
        lenders: req.body.lenders,
        cycle: req.body.cycle,
        score: req.body.score,
        returned: req.body.returned
      });
    }
  });
  handleData('添加', res);
});

// 编辑数据
app.post('/editTable', (req, res) => {
  for (let i = 0; i < tableData.data.length; i++) {
    if (tableData.data[i].id === req.body.titleId) {
      tableData.data[i].list.map(item => {
        if (item.id === req.body.id) {
          item.name = req.body.name;
          item.quotas = req.body.quotas;
          item.lenders = req.body.lenders;
          item.cycle = req.body.cycle;
          item.score = req.body.score;
          item.returned = req.body.returned;
        }
      });
    }
  }
  handleData('修改', res);
});

// 删除数据
app.delete('/delTable', (req, res) => {
  for (let i = 0; i < tableData.data.length; i++) {
    if (tableData.data[i].id === req.body.titleId) {
      tableData.data[i].list = tableData.data[i].list.filter(item => !req.body.ids.includes(item.id));
    }
  }
  handleData('删除', res);
});

// 处理数据
const handleData = (type, res) => {
  // 将文件转为 JSON 字符串
  let result = JSON.stringify(tableData);
  // 将修改后的对象存回 json 文件中
  fs.writeFile(file, result, "utf8", err => {
    if (err) {
      console.error(err);
    } else {
      // 定义一个Mock模板  
      const template = {
        code: 200,
        data: {
          msg: `${type}成功`
        }
      };

      // 使用Mock.mock生成数据  
      const data = Mock.mock(template);

      // 将数据作为JSON响应返回  
      res.json(data);
    }
  });
};

app.listen(port, () => {
  console.log(`服务启动成功 localhost:${port}或http://127.0.0.1:${port}`);
});